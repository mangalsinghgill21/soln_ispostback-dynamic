﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Soln_IsPostBack_Dynamic
{
    public partial class DefaultIsPostBackDynamic : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                
                // Binding txtbox 
                txt1.Text = "Task";
                txt2.Text = "IsPostBack";
                txt3.Text = "Dynamic on";
                txt4.Text = "Button Click";
           

            }
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            txt1.EnableViewState = false;
            txt1.Text = "see the";
            txt1.EnableViewState = true;
            txt2.Text = "powerfull";
            txt1.EnableViewState = false;
            txt3.Text = "magician";
            txt1.EnableViewState = true;
            txt4.Text = "Ispostback";

        }
    }
}